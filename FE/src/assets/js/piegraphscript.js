import Chart from "chart.js";

export default {
  name: "App",
  components: {},
  mounted: function() {
    //  piechart chart
    var ctx = document.getElementById("pieChart").getContext("2d");
    new Chart(ctx, {
      type: "pie",
        data: {
          datasets: [
            {
              data: [10, 20, 30],
              backgroundColor: [
                "rgb(255, 205,86)",
                "rgb(255, 128, 0)",
                "rgb(255, 99, 132)",
               
              ],
            },
          ],

          labels: [
					'Yellow',
					'0range',
					'Pink',
					
				
				],
        },
      options: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    });
  },
};