import { formFieldMixin } from "../../assets/js/validationscript.js";
import { mapGetters, mapActions } from "vuex";

export default {
  mixins: [formFieldMixin],
  data() {
    return {
      BookForm: {
        name: "",
        author: "",
        copies: 0,
        category_id: "",
      },
      BorrowReturnForm: {
        patron_id: "",
        copies: 0,
        book_id: "",
      },
      PatronInfo: [],
    };
  },
  methods: {
    getValue() {},
    ...mapActions([
      "fetchCategory",
      "fetchPatrons",
      "fetchBooks",
      "AddOneBook",
      "UpdateOneBook",
      "RemoveOneBook",
      "AddOneBorrowedBook",
      "AddReturnedBooks",
      "fetchBorrowedBook",
    ]),
    AddBook() {
      if (
        this.BookForm.name == "" ||
        this.BookForm.author == "" ||
        this.BookForm.copies == 0 ||
        this.BookForm.category_id == ""
      ) {
        this.ErrorToastr();
      } else {
        this.AddOneBook(this.BookForm);
        this.AddToast(this.BookForm.name, "Added Succefully");
        this.HideModal();
      }
    },
    UpdateBook() {
      if (
        this.BookForm.name == "" ||
        this.BookForm.author == "" ||
        this.BookForm.copies == 0 ||
        this.BookForm.category_id == ""
      ) {
        this.ErrorToastr();
      } else {
        this.UpdateOneBook(this.BookForm);
        this.UpdateToast(this.BookForm.name);
        this.HideModal();
      }
    },
    RemoveBook(Book) {
        this.DeleteToaster();
      this.RemoveOneBook(Book);
      this.HideModal();
    },
    AddBorrowedBook() {
      if (
        this.BorrowReturnForm.book_id == ""
      ) {
        this.ErrorToastr();
      }else if(
        this.BorrowReturnForm.patron_id == ""){
          this.ErrorToastr();     
      }else if(
        this.BorrowReturnForm.copies > this.Selected.copies){
        this.ExceedToaster();
      }else if(
        this.BorrowReturnForm.copies == 0
      ){
        this.ExceeedToaster();
      }
       else {
        this.AddOneBorrowedBook(this.BorrowReturnForm);
        this.AddToast(this.Selected.name, "Borrowed Succefully");
        this.HideModal();
      }
    },
    ReturnedBook() {
      if (
        this.BorrowReturnForm.book_id == "" ||
        this.BorrowReturnForm.patron_id == "" ||
        this.BorrowReturnForm.copies == 0 || ""
      ) {
        this.ErrorToastr();
      } else {
        this.AddReturnedBooks(this.BorrowReturnForm);
        this.AddToast(this.Selected.name, "Returned Succefully");
        this.HideModal();
        this.AllBorrowedBook;
      }
    },
  },
  mounted() {
    this.fetchBooks();
    this.fetchCategory();
    this.fetchPatrons();
    this.fetchBorrowedBook();
  },
  computed: {
    ...mapGetters([
      "LoadingStatus",
      "AllCategories",
      "AllPatrons",
      "AllBorrowedBook",
    ]),
    BookModelFilter: function() {
      var self = this;
      return this.$store.getters.AllBooks.filter(function(Book) {
        return (
          Book.name.toLowerCase().indexOf(self.Search.toLowerCase()) >= 0 ||
          Book.author.toLowerCase().indexOf(self.Search.toLowerCase()) >= 0 ||
          Book.category.category
            .toLowerCase()
            .indexOf(self.Search.toLowerCase()) >= 0
        );
      });
    },
  },
};