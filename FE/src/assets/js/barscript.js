import Chart from "chart.js";

export default {
  name: "App",
  components: {},
  mounted: function() {
    var ctx = document.getElementById("barChart").getContext("2d");
    new Chart(ctx, {
      type: "bar",
      data: {
    labels: ['January', 'February', 'March', 'April',],
        datasets: [
          {
            label: "Science Books",
            data: [7, 10, 9, 12],
            backgroundColor: "rgb(255, 205,86)",
          },
          {
            label: "History",
            data: [7, 10, 9, 12],
            backgroundColor: "rgb(255, 128, 0)",
          },
          {
            label: "Politics",
            data: [4, 7, 10, 11],
            backgroundColor: "rgb(255, 99, 132)",
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        },
      },
    });
  },
};