import BookBaseURI from "../../config/Book";

const state = {
  Books: [],
};

const getters = {
  AllBooks: (state) => state.Books,
};

const actions = {
  async fetchBooks({ commit }) {
    commit("LoadingStatus", true);
    const response = await BookBaseURI.index();
    commit("LoadingStatus", false);
    commit("Books", response.data);
  },

  async AddOneBook({ commit }, Book) {
    const response = await BookBaseURI.store(Book);
    commit("AddBook", response.data.data);
  },
  async UpdateOneBook({ commit }, Book) {
    const response = await BookBaseURI.update(Book);
    commit("UpdateBook", response.data.data);
  },
  async RemoveOneBook({ commit }, Book) {
    BookBaseURI.destroy(Book);
    commit("RemoveBook", Book.id);
  },
};

const mutations = {
  Books: (state, Books) => (state.Books = Books),
  AddBook: (state, Book) => {
    state.Books.unshift(Book);
  },
  UpdateBook: (state, Book) => {
    const index = state.Books.findIndex((item) => item.id === Book.id);
    if (index !== -1) state.Books.splice(index, 1, { ...Book });
  },
  RemoveBook: (state, Book) => {
    state.Books = state.Books.filter((item) => {
      return item.id != Book;
    });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
