import BorrowedBookBaseURI from "../../config/Borrowed_Book";

const state = {
  BorrowedBook: [],
};

const getters = {
  AllBorrowedBook: (state) => state.BorrowedBook,
};

const actions = {
  async fetchBorrowedBook({ commit }) {
    commit("LoadingStatus", true);
    const response = await BorrowedBookBaseURI.index();
    commit("LoadingStatus", false);
    commit("BorrowedBook", response.data);
  },

  async AddOneBorrowedBook({ commit, dispatch }, BorrowedBook) {
    const response = await BorrowedBookBaseURI.store(BorrowedBook);
    commit("AddBorrowedBook", response.data.data);
    dispatch("fetchBooks");
  },
};

const mutations = {
  BorrowedBook: (state, BorrowedBook) => (state.BorrowedBook = BorrowedBook),
  AddBorrowedBook: (state, BorrowedBook) => {
    state.BorrowedBook.unshift(BorrowedBook);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
