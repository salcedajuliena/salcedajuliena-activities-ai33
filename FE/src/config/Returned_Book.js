import Api from "./Api";
const Base = "Returned_Books";
export default {
  index() {
    return Api.get(Base);
  },
  store(Returned_Books) {
    return Api.post(Base, Returned_Books);
  },
};
