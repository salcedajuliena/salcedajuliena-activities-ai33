<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => ['required', Rule::unique('categories')->ignore($this->id),' min:2', 'max:255'],
        ];
    }

    public  function messages()
    {
        return [
            'category.required' => 'Category attribute is required',
            'category.unique' => 'Category already exist',
        ];
    }
}
