<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReturnedBooksRequest;
use App\Models\Books;
use App\Models\Borrowed_Books;
use App\Models\Returned_Books;

class ReturnedBookController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Returned_Books = Returned_Books::with('book','patron')->orderBy('created_at', 'desc')->get();
        return response()->json($Returned_Books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReturnedBooksRequest $request)
    { 

       
        $BookBorrowed = $BookBorrowed = Borrowed_Books::where([
            'book_id' =>  $request->book_id,
            'patron_id' => $request->patron_id ])->firstOrFail();

        $Book = Books::findOrFail($request->book_id);

        if ($BookBorrowed->copies == $request->copies) {
            $Book->copies = ( $Book->copies + $request->copies);
            $BookBorrowed->delete();
        }else{
            $Book->copies = ( $Book->copies + $request->copies);
            $BookBorrowed->update(['copies' => ($BookBorrowed->copies - $request->copies)]);
        }

        $Return = Returned_Books::create($request->validated());
        $Book->save();
        return response()->json(['message' => 'Book Successully Returend', 'data' => $Return]);

    }


      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Returned_Books $Returned_Books)
    {
        
        $Returned_Books = Returned_Books::with('book', 'patron')->get()->where('id', $Returned_Books->id);
        return response()->json($Returned_Books);
    }

}
