<?php

namespace App\Http\Controllers;

use App\Http\Requests\BorrowBookRequest;
use App\Models\Books;
use App\Models\Borrowed_Books;

class BorrowBookController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $BorrowBook = Borrowed_Books::with('book','patron')->orderBy('created_at', 'desc')->get();
        return response()->json($BorrowBook);
    }


   /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowBookRequest $request)
    {
    
        $Book = Books::findOrFail($request->book_id);

        if ($Book->copies >= $request->copies) {
            
            $Book->copies = ( $Book->copies - $request->copies);
            $Book->save();

            $BorrowBook = Borrowed_Books::create($request->validated());
            $BookBorrowed = Borrowed_Books::with('book', 'patron')->get()->where('id', $BorrowBook->id)->first();

            return response()->json(['message' => 'Borrowing Successful', 'data' => $BookBorrowed]);

        }else{
            return response()->json(['message' => 'The number of books exceed the limit']);
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Borrowed_Books $Borrowed_Book)
    {
        $Borrowed_Book = Borrowed_Books::with('book', 'patron')->get()->where('id', $Borrowed_Book->id);
        return response()->json($Borrowed_Book);
    }
}
