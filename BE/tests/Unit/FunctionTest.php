<?php

namespace Tests\Unit;

use App\Models\Books;
use App\Models\Borrowed_Books;
use App\Models\Categories;
use App\Models\Patrons;
use App\Models\Returned_Books;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;


class FunctionTest extends TestCase
{
    use RefreshDatabase;
    protected $faker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_making_an_POST_api_request_to_category()
    {
        $data = [
            'category' => 'Computer Science'
        ];

        $this->postJson(route('Categories.store'), $data)
            ->assertStatus(200)
            ->assertJson(['message' => 'Categories Saved', 'data' => $data]);
    }
    

    public function test_making_an_GET_api_request_to_category()
    {
        Categories::create([
            'category' => 'Computer Science'
        ]);
 
        $this->getJson(route('Categories.index'))
            ->assertStatus(200);
    }

    public function test_making_an_GET_specific_json_api_request_to_category()
    {
        $Category = Categories::create([
            'category' => 'Computer Science'
        ]);

       
        $this->getJson(route('Categories.show', $Category))
        ->assertStatus(200);

    }


    public function test_making_an_PUT_api_request_to_category()
    {
       $Category = Categories::create([
        'category' => 'Programming'
    ]);

        $data = [
            'category' => 'Computer Science'
        ];

        $this->putJson(route('Categories.update', $Category), $data)
            ->assertStatus(200)
            ->assertJson(['message' => 'Category Updated', 'data' => $data]);

    }

    public function test_making_an_DELETE_api_request_to_category() {

        $Category = Categories::create([
            'category' => 'Computer Science'
        ]);

        $this->deleteJson(route('Categories.destroy', $Category))
            ->assertStatus(200);

    }


    public function test_making_an_POST_api_request_to_book()
    {
        $Category =Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = [
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ];
        $this->postJson(route('Books.store'), $Books)
            ->assertStatus(200)
            ->assertJson(['message' => 'Book Saved', 'data' => $Books]);
    }

    public function test_making_an_GET_api_request_to_book()
    {
        $Category = Categories::create([
            'category' => 'Computer Science'
        ]);

        Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $this->getJson(route('Books.index'))
            ->assertStatus(200);
    }

    public function test_making_an_GET_specific_json_api_request_to_book()
    {
        $Category = Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $this->getJson(route('Books.show', $Books))
        ->assertStatus(200);

    }

    public function test_making_an_PUT_api_request_to_book()
    {
       $Category = Categories::create([
        'category' => 'Computer Science'
    ]);

        $data = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $Books = [
            'name' => 'Clean Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
         
        ];

        $this->putJson(route('Books.update', $data), $Books)
            ->assertStatus(200)
            ->assertJson(['message' => 'Book Updated', 'data' => $Books]);

    }

    public function test_making_an_DELETE_api_request_to_book() {

        $Category = Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);
        $this->deleteJson(route('Books.destroy', $Books))
            ->assertStatus(200);
    }


    public function test_making_an_POST_api_request_to_patron()
    {

        $Patron = [
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar242@gmail.com'

        ];
        $this->postJson(route('Patrons.store'), $Patron)
            ->assertStatus(200)
            ->assertJson(['message' => 'Patron Saved', 'data' => $Patron]);
    }

    public function test_making_an_GET_api_request_to_patron()
    {
    
        Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar242@gmail.com'
        ]);

        $this->getJson(route('Patrons.index'))
            ->assertStatus(200);
    }

    public function test_making_an_GET_specific_json_api_request_to_patron()
    {
       $Patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar242@gmail.com'
        ]);

        $this->getJson(route('Patrons.show', $Patron))
        ->assertStatus(200);

    }

    public function test_making_an_PUT_api_request_to_patron()
    {


        $data = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'
        ]);

        $Patron = [
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar@gmail.com' 
        ];

        $this->putJson(route('Patrons.update', $data), $Patron)
            ->assertStatus(200)
            ->assertJson(['message' => 'Patron Updated', 'data' => $Patron]);

    }
    public function test_making_an_DELETE_api_request_to_patron() {


        $Patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'
        ]);

        $this->deleteJson(route('Patrons.destroy', $Patron))
            ->assertStatus(200);
    }

    public function test_making_an_POST_api_request_to_borrow_book()
    {
        $Category =Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $Patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar242@gmail.com'
        ]);

        $BorrowedBook = [
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ];

        $this->postJson(route('Borrowed_Book.store'), $BorrowedBook)
            ->assertStatus(200);
            
    }
    public function test_making_an_GET_api_request_to_borrow_book()
    {
        $Category =Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $Patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar242@gmail.com'
        ]);
        Borrowed_Books::create([
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ]);

        $this->getJson(route('Borrowed_Book.index'))
            ->assertStatus(200);
    }

    public function test_making_an_GET_specific_json_api_request_to_borrow_book()
    {
        $Category =Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $Patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar242@gmail.com'
        ]);
       $BorrowBook =  Borrowed_Books::create([
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ]);

      
        $this->getJson(route('Borrowed_Book.show', $BorrowBook))
        ->assertStatus(200);

    }


    public function test_making_an_POST_api_request_to_return_book()
    {
        $Category =Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $Patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar@gmail.com'
        ]);

        Borrowed_Books::create([
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ]);

       $ReturnBook =  [
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ];

        $this->postJson(route('Returned_Books.store'), $ReturnBook)
            ->assertStatus(200);
            
    }

    public function test_making_an_GET_api_request_to_return_book()
    {
        $Category =Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $Patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar@gmail.com'
        ]);
        Borrowed_Books::create([
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ]);

        
        Returned_Books::create([
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ]);

        $this->getJson(route('Returned_Books.index'))
            ->assertStatus(200);
    }

    public function test_making_an_GET_specific_json_api_request_to_return_book()
    {
        $Category =Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $Patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar@gmail.com'
        ]);
       Borrowed_Books::create([
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ]);

       $ReturnBook =  Returned_Books::create([
            'patron_id' => $Patron->id,
            'copies' => rand(1, 5),
            'book_id' => $Books->id 

        ]);
      
        $this->getJson(route('Returned_Books.show', $ReturnBook))
        ->assertStatus(200);

    }

}
