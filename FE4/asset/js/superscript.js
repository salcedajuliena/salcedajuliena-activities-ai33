		var barChartData = {
			labels: ['January', 'February', 'March', 'April',],
			datasets: [{
				label: 'Science Books',
				backgroundColor: window.chartColors.yellow,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'History',
				backgroundColor: window.chartColors.red,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Politics',
				backgroundColor: window.chartColors.blue,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};


		
		var pieChartData = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						
					],
					backgroundColor: [
						window.chartColors.yellow,
						window.chartColors.red,
						window.chartColors.blue,
						
						
					],
					label: 'Dataset 1'
				}],
				labels: [
					'yellow',
					'red',
					'blue',
					'',
					
				]
			},
			options: {
				responsive: true
			}
		};

                var pieChart = document.getElementById('chart-area').getContext('2d');
                new Chart(ctx, pieChartData);



		
