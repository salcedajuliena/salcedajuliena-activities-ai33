var users = [
    {
        id: 1,
        name: "Jul",
       

        middlename: "Eligan",
        lastname: "Salceda",
        email: "salcedajuliena@gmail.com",
        date: "January 1, 2020",

    },
    {
        id: 2,
        name: "Jols",
        middlename: "Eligan",
          email: "salcedajuliena@gmail.com",
        lastname: "Salceda",
        date: "February 25, 2020",
        
    },
	{
        id: 3,
        name: "Mau",
        email: "salcedajuliena@gmail.com",
        date: "March 1, 2020",
        middlename: "Eligan",
        lastname: "Salceda",
        
    }
];

$("#overlay").hide();



$("#kape").click(function(){
    $("#overlay").toggle();
});

$("#closed").click(function(){
    $("#overlay").toggle();
});
$("#kope").click(function(){
    $("#overlay").toggle();
});

$.each(users, function (i, user) {
    appendToUsrTable(user);
});

$("form").submit(function (e) {
    e.preventDefault();
});
function deleteUser(id) {
    var action = confirm("Are you sure?");
    var msg = "Contact deleted successfully!";
    users.forEach(function (user, i) {
        if (user.id == id && action != false) {
            users.splice(i, 1);
            $("#userTable #user-" + user.id).remove();
            flashMessage(msg);
        }
    });
}


function flashMessage(msg) {
    $(".flashMsg").remove();
    $(".row").prepend(`
         
      `);
}

function appendToUsrTable(user) {
    $("#userTable > tbody:last-child").append(`
          <tr id="user-${user.id}" class="text-center">
              <td class="userData" name="name">${user.name}   ${user.middlename}  ${user.lastname}</td>
              '<td class="userData" name="email">${user.email} </td>
               '<td class="userData" name="email">${user.date}</td>
               '<td align="center">
                  <a href="#" class="text-success"><img src ="asset/images/editicon.png" height="32" width="32" onClick="deleteUser(${user.id})"></a>
              </td>
              '<td align="center">
                  <a href="#" class="text-success"><img src ="asset/images/deleteicon.png" height="32" width="32" onClick="deleteUser(${user.id})"></a>
              </td>
          </tr>
      `);
}
